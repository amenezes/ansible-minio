Ansible Role: Minio
===================

[![build status](https://gitlab.com/amenezes/ansible-minio/badges/master/build.svg)](https://gitlab.com/amenezes/ansible-minio/commits/master)

Install and configure the [Minio](https://minio.io/) S3 compatible object storage server
on RHEL/CentOS and Debian/Ubuntu.

Requirements
---

None.

Role Variables
---

Available variables are listed below, along with default values (see `defaults/main.yml`):
```yml
minio_server_bin: /usr/local/bin/minio
minio_client_bin: /usr/local/bin/mc
```
Installation path of the Minio server and client binaries.
```yml
minio_user: minio
minio_group: minio
```
Name and group of the user running the minio server.
**NB**: This role automatically creates the minio user and/or group if these does not exist in the system.
```yml
minio_server_envfile: /etc/default/minio
```
Path to the file containing the minio server configuration ENV variables.
```yml
minio_server_addr: ":9000" #default
```
The Minio server listen address.
```yml
minio_server_datadirs: [ ]
```
Directories of the folder containing the minio server data
**NB**: This variable must always be set by the role, otherwise the minio service will not start.
```yml
minio_server_opts: ""
```
Additional CLI options that must be appended to the minio server start command.
```yml
minio_access_key: ""
minio_secret_key: ""
```
Minio access and secret keys.
```yml
skip_server: False
skip_client: False
```
Switches to disable minio server and/or minio client installation.

Dependencies
---

None.

Example Playbook
---
```yml
- name: "Install Minio"
    hosts: all
```
         
License
---

GPLv2


repositório original: Andrea Tosatto ([@\_hilbert\_](https://twitter.com/_hilbert_))
